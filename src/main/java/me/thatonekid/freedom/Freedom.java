package me.thatonekid.freedom;

import me.thatonekid.freedom.command.Command_freedom;
import me.thatonekid.freedom.listeners.ChatListener;
import net.pravian.aero.command.handler.AeroCommandHandler;
import net.pravian.aero.command.handler.SimpleCommandHandler;
import net.pravian.aero.config.YamlConfig;
import net.pravian.aero.plugin.AeroPlugin;
import net.pravian.aero.util.Loggers;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;

import java.util.HashMap;

public class Freedom extends AeroPlugin<Freedom> {

    public static Freedom plugin;

    public static AeroCommandHandler handler;

    public Loggers logger;

    public YamlConfig config;

    public YamlConfig staff;

    public static HashMap<Player, Boolean> adminchat = new HashMap<Player, Boolean>();

    public void load() {
        plugin = this;
        Loggers.info(plugin.getName() + " is now loading!");
        config = new YamlConfig(plugin, "config.yml", true);
        staff = new YamlConfig(plugin, "staff.yml", true);

    }

    public void enable() {
        plugin = this;
        config.load();
        staff.load();

        PluginManager pm = getServer().getPluginManager();

        pm.registerEvents(new ChatListener(), this);

        handler = new SimpleCommandHandler(plugin);
        handler.setCommandClassPrefix("Command_");
        handler.loadFrom(Command_freedom.class.getPackage());
        handler.registerAll();

        Loggers.info(plugin.getName() + " is now enabled!");
        Loggers.info(plugin.getName() + " v" + plugin.getVersion() + " was created by " + plugin.getAuthor());
    }

    public void disable() {
        Loggers.info(plugin.getName() + " has been disabled.");
    }

    public YamlConfig getStaff() { return staff; }
}
