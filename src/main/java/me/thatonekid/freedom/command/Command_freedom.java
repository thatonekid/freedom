package me.thatonekid.freedom.command;

import me.thatonekid.freedom.Freedom;
import me.thatonekid.freedom.rank.Rank;
import net.pravian.aero.command.CommandOptions;
import net.pravian.aero.command.SimpleCommand;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

@CommandOptions(description = "The default command for Freedom")

public class Command_freedom extends SimpleCommand<Freedom> {


    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        msg(sender, "Freedom is an all op based plugin, created by ThatOneKid.", ChatColor.GOLD);
        msg(sender, "This server is running " + sender.getServer().getBukkitVersion(), ChatColor.GOLD);
        return true;
    }
}
