package me.thatonekid.freedom.rank;

import me.thatonekid.freedom.Freedom;
import net.pravian.aero.config.YamlConfig;
import org.bukkit.entity.Player;

public class Staff {

    public static void addAdmin(Player player)
    {
        YamlConfig staff = Freedom.plugin.staff;
        staff.createSection(player.getUniqueId().toString());
        staff.set(player.getUniqueId().toString() + ".rank", "super admin");
        staff.set(player.getUniqueId().toString() + ".name", player.getName());
                staff.save();
    }
}
