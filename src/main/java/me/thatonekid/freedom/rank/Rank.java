package me.thatonekid.freedom.rank;

import me.thatonekid.freedom.Freedom;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public enum Rank {

    IMPOSTER("Imposter", ChatColor.YELLOW + "" + ChatColor.BOLD + "IMPOSTER", -1),
    OP("Op", ChatColor.BOLD + "" + ChatColor.RED + "OP", 0),
    SUPERADMIN("Super Admin", ChatColor.AQUA + "" + ChatColor.BOLD + "SUPER", 1),
    TELNETADMIN("Telnet Admin", ChatColor.DARK_GREEN + "" + ChatColor.BOLD + "TELNET", 2),
    SENIORADMIN("Senior Admin", ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "SENIOR", 3),
    DEVELOPER("Developer", ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + "DEV", 4),
    OWNER("Owner", ChatColor.BLUE + "" + ChatColor.BOLD + "OWNER", 5),
    CONSOLE("Console", ChatColor.DARK_RED + "" + ChatColor.BOLD + "CONSOLE", 6);


    private final String name;
    private final String prefix;
    private final int level;

    private Rank(String name, String prefix, int level)
    {
        this.name = name;
        this.prefix = prefix;
        this.level = level;

    }

    final static Freedom plugin = me.thatonekid.freedom.Freedom.getPlugin(Freedom.class);

    public int getLevel()
    {
        return level;
    }

    public boolean isAbout(Rank rank) {
        return level >= rank.getLevel();
    }
    public String getName()
    {
        return name;
    }
    public String getPrefix(){
        return prefix;
    }




    public static boolean isAdmin(CommandSender sender)
    {
        Player player = (Player) sender;
        return plugin.getStaff().contains(player.getUniqueId().toString());
    }

    public static boolean isTelnet(CommandSender sender)
    {
        Player player = (Player) sender;
        return isAdmin(player) && plugin.getStaff().getString(player.getUniqueId().toString() + ".rank").equalsIgnoreCase("telnet admin");
    }
    public static boolean isSenior(CommandSender sender)
    {
        Player player = (Player) sender;
        return isAdmin(player) && plugin.getStaff().getString(player.getUniqueId().toString() + ".rank").equalsIgnoreCase("senior admin");
    }
    public static boolean isDev(CommandSender sender)
    {
        Player player = (Player) sender;
        return isAdmin(player) && plugin.getStaff().getString(player.getUniqueId().toString() + ".rank").equalsIgnoreCase("developer");
    }
    public static boolean isOwner(CommandSender sender)
    {
        Player player = (Player) sender;
        return isAdmin(player) && plugin.getStaff().getString(player.getUniqueId().toString() + ".rank").equalsIgnoreCase("owner");
    }
    public static boolean isConsole(CommandSender sender)
    {
        Player player = (Player) sender;
        return isAdmin(player) && plugin.getStaff().getString(player.getUniqueId().toString() + ".rank").equalsIgnoreCase("console");
    }

    public static Rank findRank(String string)
    {
        try
        {
            return Rank.valueOf(string.toUpperCase());
        }
        catch (Exception ignored)
        {
        }

        return Rank.OP;
    }

    public static Rank getRank(CommandSender sender)
    {
        if (!(sender instanceof Player))
        {
            return Rank.CONSOLE;
        }
        final Player player = (Player) sender;
        if (isAdmin(player))
        {
            if (isTelnet(player))
            {
                return Rank.TELNETADMIN;
            }
            if (isSenior(player))
            {
                return Rank.SENIORADMIN;
            }
            if (isDev(player))
            {
                return Rank.DEVELOPER;
            }
            if (isOwner(player))
            {
                return Rank.OWNER;
            }
            return Rank.SUPERADMIN;

        }

        return Rank.OP;
    }







}